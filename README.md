# Docker_m4_toolkit
*Copyright (c) 2016, Victor Arribas*
*https://bitbucket.org/chrooting-varribas/docker_m4_toolkit.git*

A tiny but powerful toolkit to add code reuse, fragments and templating support to Docker.

It comes with two sample fragments: apt-fast and user-layer.

**Requires: docker, m4**

## Demo example
```
mkdir ~/demo && cd ~/demo
cat<<EOF>Dockerfile.m4

FROM ubuntu:14.04

include(fragments/apt-fast)
RUN apt-fast install whatever

include(fragments/user-layer)
RUN whoami

EOF

alias dt=docker_m4_toolkit
dt make && dt build_devbox
docker images
```


------------------------------------------------------------


# Docker knowledge

## Install Docker for Ubuntu 14.04 (Trusty)
```
#!/bin/sh

# Install Docker for Ubuntu 14.04 (Trusty)
apt-key adv --keyserver hkp://p80.pool.sks-keyservers.net:80 --recv-keys 58118E89F3A912897C070ADBF76221572C52609D
echo "deb https://apt.dockerproject.org/repo ubuntu-`lsb_release -cs` main">/etc/apt/sources.list.d/docker.list

apt-get update
apt-get install docker-engine

service docker start
update-rc.d docker defaults

docker info | grep Storage
cat<<EOF
Add every plain-user to docker group with:
usermod -aG docker <username>
EOF
```

# Basic Info
## AUFS
https://docs.docker.com/engine/userguide/storagedriver/aufs-driver/

## Install
https://docs.docker.com/engine/installation/ubuntulinux/

## Use
https://docs.docker.com/engine/userguide/
  https://docs.docker.com/engine/userguide/dockerizing/
  https://docs.docker.com/engine/userguide/dockerimages/

## Push
https://docs.docker.com/linux/step_six/

## Docker compose
https://docs.docker.com/compose/
  https://docs.docker.com/compose/compose-file/
(alternative) https://github.com/avirshup/DockerMake

## Press notes
http://gernotklingler.com/blog/docker-replaced-virtual-machines-chroots/


# Docker concepts
0. Best practices
   https://docs.docker.com/engine/articles/dockerfile_best-practices/

1. ENTRYPOINT vs CMD
   https://www.ctl.io/developers/blog/post/dockerfile-entrypoint-vs-cmd/
   http://stackoverflow.com/questions/21553353/what-is-the-difference-between-cmd-and-entrypoint-in-a-dockerfile

2. ADD vs COPY
   https://www.ctl.io/developers/blog/post/dockerfile-add-vs-copy/
   https://docs.docker.com/engine/reference/builder/#copy

3. Data Volumes
   https://docs.docker.com/engine/userguide/dockervolumes/

4. docker exec: Enter into running container
   http://stackoverflow.com/questions/20932357/docker-enter-running-container-with-new-tty


## Docker tips and tricks
1. 10 docker tips and tricks
   https://nathanleclaire.com/blog/2014/07/12/10-docker-tips-and-tricks-that-will-make-you-sing-a-whale-song-of-joy/
   * Bind mount the docker socket on docker run
   * Use containers as highly disposable dev environments
   * Super easy terminals in-browser with wetty

2. Writing Docker files
   https://kimh.github.io/blog/en/docker/gotchas-in-writing-dockerfile-en/
   * cmd vs entrypoint
   * write Dockerfile


# Docker Advanced
## Docker cleanup: remove unwanted containers & images
1. http://jimhoskins.com/2013/07/27/remove-untagged-docker-images.html
  * remove stopped containers
    docker rm $(docker ps -a -q)
  * remove untagged images
    docker rmi $(docker images | grep "^<none>" | awk "{print $3}")
2. https://sosedoff.com/2013/12/17/cleanup-docker-containers-and-images.html

## X11 forwarding
http://fabiorehm.com/blog/2014/09/11/running-gui-apps-with-docker/
http://stackoverflow.com/questions/25281992/alternatives-to-ssh-x11-forwarding-for-docker-containers
```
docker run|create \
  --env "DISPLAY" \
  --net host \
  -v /tmp/.X11-unix:/tmp/.X11-unix:ro \
  -v $HOME/.Xauthority:/home/$user/.Xauthority:ro \
  -u $user
```

## OpenGL support
http://gernotklingler.com/blog/howto-get-hardware-accelerated-opengl-support-docker/
  https://github.com/gklingler/docker3d/blob/master/ubuntuWithNvidiaDriver/Dockerfile
https://github.com/ojgarciab/docker-gazebo-5/issues/3#issuecomment-174193391
  https://github.com/ojgarciab/docker3d/blob/master/create.sh

## Docker automatition: include & Makefiles
http://bobbynorton.com/posts/includes-in-dockerfiles-with-m4-and-make/
http://www.itnotes.de/docker/development/tools/2014/08/31/speed-up-your-docker-workflow-with-a-makefile/
https://ypereirareis.github.io/blog/2015/05/04/docker-with-shell-script-or-makefile/
## By example
https://github.com/sameersbn/docker-browser-box/blob/master/Makefile
```
$ cat Dockerfile.m4:
FROM ubuntu:trusty
ENV DEBIAN_FRONTEND noninteractive
include(ruby_2_1_2.m4)

$ cat makefile
lib = ./dockerfiles
dockerfile: $(lib)/*.m4
    m4 -I $(lib) $(lib)/Dockerfile.m4 > Dockerfile
build: dockerfile
    docker build --rm -t your/image .
```
Note: tried with gcc, but Docker comment escape char is '#' and collides with macro specification.

## Docker squashing: merge hyper-fragmented images
http://jasonwilder.com/blog/2014/08/19/squashing-docker-images/
https://github.com/jwilder/docker-squash
docker save <hash> | sudo docker-squash -verbose -t jwilder/whoami:squash -from <hash>  | docker load

## Docker build concerns: best practices & minimal images
https://www.dajobe.org/blog/2015/04/18/making-debian-docker-images-smaller/


# Docker throubleshooting
## --hostname (-h) & --net=host are not compatible
https://github.com/docker/docker/issues/17147

## no interactive shell (no tty)
https://github.com/docker/docker/issues/728

## Xt error: Can't open display
Several motivations: 
a) DISPLAY not set
b) .X11-unix or .Xauthority not found
c) different network that host (requires --net=host)


# DockerHub
## Must-to-use containers 
busybox - https://hub.docker.com/r/library/busybox/
nginx - https://hub.docker.com/r/library/nginx/

# ToDo
docker-compose.yml