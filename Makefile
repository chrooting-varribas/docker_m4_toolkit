# Copyright (c) 2016
# Author: Victor Arribas <v.arribas.urjc@gmail.com>
# License: GPLv3 <http://www.gnu.org/licenses/gpl-3.0.html>
#
# Manpages:
# http://makepp.sourceforge.net/1.19/makepp_tutorial.html
# http://nuclear.mutantstargoat.com/articles/make/#writing-install-uninstall-rules

PREFIX = /usr

.PHONY: install
install:
	install -m 0755 docker_m4_toolkit $(DESTDIR)$(PREFIX)/bin
	mkdir -m 755 -p $(DESTDIR)/etc/docker_m4_toolkit/fragments
	install -m 0644 fragments/* $(DESTDIR)/etc/docker_m4_toolkit/fragments

alias:
	ln -s $(DESTDIR)$(PREFIX)/bin/docker_m4_toolkit $(DESTDIR)$(PREFIX)/bin/dt

.PHONY: uninstall
uninstall:
	rm -f $(DESTDIR)$(PREFIX)/bin/docker_m4_toolkit
	find $(DESTDIR)/etc/docker_m4_toolkit -delete

.PHONY: reinstall
reinstall: uninstall install

